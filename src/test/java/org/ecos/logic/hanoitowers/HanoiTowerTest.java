package org.ecos.logic.hanoitowers;

import org.ecos.logic.hanoitowers.exception.NotAllowedException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.ecos.logic.hanoitowers.RowType.*;

class HanoiTowerTest {

    @Test
    void gettingTheLastDiskOnTheTowerWhenTheTowerIsEmptyWillReturnZERO() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower();

        RowType result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(ZERO);
    }

    @Test
    void gettingTheLastDiskOnTheTowerWhenTheTowerHasOnlyOneRow() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower(ZERO,ZERO,ZERO,ZERO,FIVE);

        RowType result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(FIVE);
    }

    @Test
    void gettingTheLastDiskOnTheTowerWhenTheTowerHasTwoDisks() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower(ZERO,ZERO,ZERO,TWO,THREE);

        RowType result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(TWO);
    }

    @Test
    void gettingTheLastDiskOnTheTowerWhenTheTowerHasThreeDisks() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower(ZERO,ZERO,ONE,TWO,THREE);

        RowType result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(ONE);
    }

    @Test
    void whenIAddADiskTheTowerAlwaysAddsItInTheLastEmptyPlace() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower();

        towerOnTest.add(ONE);

        RowType result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(ONE);
    }

    @Test
    void ICanNotAddADiskEqualOrGreaterThanTheLastDiskOnTheTower() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower();

        towerOnTest.add(ONE);
        assertThatThrownBy(()->towerOnTest.add(ONE)).isInstanceOf(NotAllowedException.class);
        assertThatThrownBy(()->towerOnTest.add(TWO)).isInstanceOf(NotAllowedException.class);
        assertThatThrownBy(()->towerOnTest.add(THREE)).isInstanceOf(NotAllowedException.class);
        assertThatThrownBy(()->towerOnTest.add(FOUR)).isInstanceOf(NotAllowedException.class);
        assertThatThrownBy(()->towerOnTest.add(FIVE)).isInstanceOf(NotAllowedException.class);
    }
    @Test
    void ICanNotAddZeroValueDisk() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower();

        assertThatThrownBy(()->towerOnTest.add(ZERO)).isInstanceOf(NotAllowedException.class);
    }

    @Test
    void whenIPopADiskItWillBeRemovedFromItsTowerAndReturnsTheRowType() throws NotAllowedException {

        HanoiTower towerOnTest = new HanoiTower();

        towerOnTest.add(THREE);
        towerOnTest.add(TWO);
        towerOnTest.add(ONE);

        RowType result = towerOnTest.pop();
        assertThat(result).isEqualTo(ONE);

        result = towerOnTest.getLastDisk();
        assertThat(result).isEqualTo(TWO);
    }

    @Test
    void aTowerIsCompletedWhenHasAllTheDiskInItsRightOrder() throws NotAllowedException {
        HanoiTower towerOnTest = new HanoiTower(ONE, TWO, THREE, FOUR, FIVE);

        assertThat(towerOnTest.isCompleted()).isTrue();
    }
}
