package org.ecos.logic.hanoitowers;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ecos.logic.hanoitowers.RowType.*;


class TextGraphicTest {

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingZeroElements(){
         List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(ZERO);

         assertThat(Arrays.asList(" "," "," "," "," ","|"," "," "," "," "," ")).isEqualTo((result));
    }

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingOneElements(){
        List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(ONE);

        assertThat(Arrays.asList(" "," "," "," ","*","|","*"," "," "," "," ")).isEqualTo((result));
    }

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingTwoElements(){
        List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(TWO);

        assertThat(Arrays.asList(" "," "," ","*","*","|","*","*"," "," "," ")).isEqualTo((result));
    }

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingThreeElements(){
        List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(THREE);

        assertThat(Arrays.asList(" "," ","*","*","*","|","*","*","*"," "," ")).isEqualTo((result));
    }

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingFourElements(){
        List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(FOUR);

        assertThat(Arrays.asList(" ","*","*","*","*","|","*","*","*","*"," ")).isEqualTo((result));
    }

    @Test
    void dynamicGenerateTowerWithZeroToFiveElements_PassingFiveElements(){
        List<String> result = TextGraphic.dynamicGenerateTowerWithZeroToFiveElements(FIVE);

        assertThat(Arrays.asList("*","*","*","*","*","|","*","*","*","*","*")).isEqualTo((result));
    }


}
