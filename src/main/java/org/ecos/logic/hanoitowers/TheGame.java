package org.ecos.logic.hanoitowers;

import org.ecos.logic.hanoitowers.exception.NotAllowedException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static org.ecos.logic.hanoitowers.RowType.*;
import static org.ecos.logic.hanoitowers.TextGraphic.dynamicGenerateTowerWithZeroToFiveElements;

public class TheGame {

    public static void main(String[] args) throws NotAllowedException, IOException {

        HanoiTower towerOne = new HanoiTower(ONE, TWO, THREE, FOUR, FIVE);
        HanoiTower towerTwo = new HanoiTower();
        HanoiTower towerThree = new HanoiTower();

        boolean isTowerThreeCompleted = false;
        while(!isTowerThreeCompleted) {

            String dynamicStateTowersRepresentation = generateRepresentation(towerOne,towerTwo,towerThree);
            System.out.println();
            System.out.println(dynamicStateTowersRepresentation);
            System.out.println();

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String result = br.readLine();
            result = result.trim();
            int fromAsInt = Integer.parseInt(result);
            if (fromAsInt < 0 || fromAsInt > 3)
                break;

            RowType disk = null;
            if (fromAsInt == 1)
                disk = towerOne.pop();
            if (fromAsInt == 2)
                disk = towerTwo.pop();
            if (fromAsInt == 3)
                disk = towerThree.pop();

            result = br.readLine();
            result = result.trim();
            int toAsInt = Integer.parseInt(result);

            if (toAsInt < 0 || toAsInt > 3)
                break;

            if (toAsInt == 1)
                towerOne.add(disk);
            if (toAsInt == 2)
                towerTwo.add(disk);
            if (toAsInt == 3)
                towerThree.add(disk);

            isTowerThreeCompleted = towerThree.isCompleted();

        }

        System.out.println("CONTRATULATIONS!!!!!");
    }

    private static String generateRepresentation(HanoiTower towerOne, HanoiTower towerTwo, HanoiTower towerThree) {
        String dynamicStateTowersRepresentation =
                "          |                    |                    |          \n" +
                        "          |                    |                    |          \n" +
                        "          |                    |                    |          \n" +
                        "     %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s     \n" +
                        "     %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s     \n" +
                        "     %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s     \n" +
                        "     %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s     \n" +
                        "     %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s          %s%s%s%s%s%s%s%s%s%s%s     \n" +
                        "===============================================================\n";
        List<List<String>> towerOneRepresentation = new ArrayList<>();
        List<List<String>> towerTwoRepresentation = new ArrayList<>();
        List<List<String>> towerThreeRepresentation = new ArrayList<>();

        for (int i=0;i<5;i++) {
            towerOneRepresentation.add(new ArrayList<>(dynamicGenerateTowerWithZeroToFiveElements(towerOne.getRowByIndex(i))));
            towerTwoRepresentation.add(new ArrayList<>(dynamicGenerateTowerWithZeroToFiveElements(towerTwo.getRowByIndex(i))));
            towerThreeRepresentation.add(new ArrayList<>(dynamicGenerateTowerWithZeroToFiveElements(towerThree.getRowByIndex(i))));
        }

        List<String> objectRepresentation = new ArrayList<>();
        for (int i = 0; i < towerOneRepresentation.size(); i++) {
            objectRepresentation.addAll(towerOneRepresentation.get(i));
            objectRepresentation.addAll(towerTwoRepresentation.get(i));
            objectRepresentation.addAll(towerThreeRepresentation.get(i));
        }

        dynamicStateTowersRepresentation =
                String.format(dynamicStateTowersRepresentation,
                        objectRepresentation.toArray()

                );
        return dynamicStateTowersRepresentation;
    }
}
