package org.ecos.logic.hanoitowers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TextGraphic {
    public static List<String> dynamicGenerateTowerWithZeroToFiveElements(RowType fromZeroToFiveValue) throws IndexOutOfBoundsException {
        List<String> resultRight = new ArrayList<>();
        for(int i=0;i<5;i++){
            if(i<fromZeroToFiveValue.getValue()){
                resultRight.add("*");
            }else{
                resultRight.add(" ");
            }
        }
        List<String> resultLeft = new ArrayList<>(resultRight);
        Collections.reverse(resultLeft);
        resultLeft.add("|");
        resultLeft.addAll(resultRight);

        return resultLeft;
    }
}
