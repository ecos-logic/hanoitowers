package org.ecos.logic.hanoitowers;

import org.ecos.logic.hanoitowers.exception.NotAllowedException;

import static org.ecos.logic.hanoitowers.RowType.*;

public class HanoiTower {
    private final RowType[] rows = {ZERO, ZERO, ZERO, ZERO, ZERO};
    public HanoiTower(RowType firstRow, RowType secondRow, RowType thirdRow, RowType fourthRow, RowType fifthRow) throws NotAllowedException {
        this.add(fifthRow,true);
        this.add(fourthRow,true);
        this.add(thirdRow,true);
        this.add(secondRow,true);
        this.add(firstRow,true);
    }

    public HanoiTower() throws NotAllowedException {
        this(ZERO, ZERO, ZERO, ZERO, ZERO);
    }

    public RowType getRowByIndex(int rowIndex) {
        return this.rows[rowIndex];
    }

    public RowType getLastDisk() {
        for (RowType row : this.rows) {
            if (row != ZERO)
                return row;
        }
        return ZERO;
    }

    public void add(RowType rowType) throws NotAllowedException {
        add(rowType,false);
    }

    private void add(RowType rowType, boolean ableToAddZERO) throws NotAllowedException {
        if(rowType.equals(ZERO) && !ableToAddZERO)
            throw new NotAllowedException();

        boolean alreadyAdded = false;
        for (int i = 0; (i < this.rows.length && !alreadyAdded); i++) {
            if(this.rows[i]!=ZERO) {
                if(rowType.getValue()>=this.rows[i].getValue()){
                    throw new NotAllowedException();
                }
                rows[i - 1] = rowType;
                alreadyAdded = true;
            }else {
                if (i == 4 && this.rows[4] == ZERO)
                    rows[4] = rowType;
            }
        }


    }

    public RowType pop() {
        for (int i = 0; i < this.rows.length; i++) {
            if(this.rows[i]!=ZERO) {
                RowType result = this.rows[i];
                this.rows[i]=ZERO;
                return result;
            }
        }

        return ZERO;
    }

    public boolean isCompleted() {
        return
                this.getRowByIndex(4) == FIVE &&
                this.getRowByIndex(3) == FOUR &&
                this.getRowByIndex(2) == THREE &&
                this.getRowByIndex(1) == TWO &&
                this.getRowByIndex(0) == ONE
                ;
    }
}
