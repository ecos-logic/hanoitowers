package org.ecos.logic.hanoitowers;

public enum RowType {
    ZERO(0),
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5);

    private final int rowValue;

    RowType(int rowValue) {

        this.rowValue = rowValue;
    }

    public int getValue() {
        return rowValue;
    }
}
